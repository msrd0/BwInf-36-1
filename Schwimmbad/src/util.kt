/*
 36. BwInf Runde 1
 Copyright (C) 2017 Dominic Meiser
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
*/
package msrd0.bwinf36.schwimmbad

fun ceil(a : Double) = Math.ceil(a).toInt()

infix fun Int.EUR(cent : Int) = this * 100 + cent

fun <T> collectionOf(size : Int, factory : (Int) -> T) : Collection<T>
{
	return object : AbstractCollection<T>()
	{
		override val size get() = size
		override fun iterator() : Iterator<T>
			= object : Iterator<T>
			{
				var index = 0
				override fun hasNext() = index < size
				override fun next() = factory(index++)
			}
	}
}
