/*
 36. BwInf
 Copyright (C) 2017 Dominic Meiser
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
*/
package msrd0.bwinf36.test

import msrd0.bwinf36.autoscrabble.*
import org.hamcrest.MatcherAssert.*
import org.slf4j.*
import org.testng.Assert.*
import org.testng.annotations.*
import java.io.*
import java.nio.file.*

private val logger : Logger = LoggerFactory.getLogger(AutoScrabbleTest::class.java)

class AutoScrabbleTest
{
	fun bfs(str : String) : Iterable<NumberPlate>?
	{
		val p = Processor(str)
		val res = p.bfs()
		logger.debug("Die maximale Größe der Queue betrug ${p.maxQ} für ${p.str}. Es wurden ${p.numBurned} Möglichkeiten übersprungen")
		return res
	}
	
	fun check(str : String, res : Iterable<NumberPlate>)
	{
		var out : String = ""
		for (plate in res)
		{
			val plateStr = plate.toString() // because the number is random each time
			assertThat("City of $plateStr is valid",
					NumberPlate.cities.contains(plate.city))
			assertThat("Text of $plateStr is valid",
					plate.text.matches(Regex("[A-ZÄÖÜß]{1,2}")))
			out += "${plate.city}${plate.text}"
		}
		assertEquals(str, out)
	}
	
	@Test fun heike()
	{
		val res = bfs("HEIKE")
		assertNotNull(res)
		check("HEIKE", res!!)
	}
	
	@Test fun timo()
	{
		val res = bfs("TIMO")
		assertNull(res)
	}
	
	@Test fun tipp()
	{
		val res = bfs("TIPP")
		assertNull(res)
	}
	
	@Test fun tim()
	{
		val res = bfs("TIM")
		assertNull(res)
	}
	
	@Test fun ok()
	{
		val res = bfs("OK")
		assertNull(res)
	}
	
	@Test fun germanysDebts()
	{
		// http://www.sprachlog.de/2013/06/05/das-neue-laengste-wort-des-deutschen/
		val str = "ZWEIBILLIONENEINHUNDERTFUENFUNDZWANZIGMILLIARDENEINHUNDERTFUENFUNDZWANZIGMILLIONENEINHUNDERTACHTTAUSENDZWEIHUNDERTZWEIUNDDREISSIGEURO"
		val res = bfs(str)
		assertNotNull(res)
		check(str, res!!)
	}
	
	@Test fun examples()
	{
		val file = BufferedReader(InputStreamReader(ClassLoader.getSystemResourceAsStream("autoscrabble.txt")))
		var line : String
		while (true)
		{
			line = file.readLine()?.trim() ?: break
			if (line.isEmpty())
				continue
			
			val res = bfs(line)
			if (res != null)
				check(line, res)
		}
	}
	
	@BeforeTest fun loadCities()
	{
		NumberPlate.loadCities()
	}
}
