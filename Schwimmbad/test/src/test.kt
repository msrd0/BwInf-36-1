/*
 36. BwInf Runde 1
 Copyright (C) 2017 Dominic Meiser
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
*/
package msrd0.bwinf36.test

import msrd0.bwinf36.schwimmbad.*
import org.hamcrest.MatcherAssert.*
import org.hamcrest.Matchers.*
import org.testng.annotations.Test

val defaultPriceList : PriceList = defaultPrices()

fun dfs(ages : Collection<Int>, vouchers : Int, state : State, prices : PriceList = defaultPriceList) : Result
{
	println("Running dfs($ages, $vouchers, $state)")
	val p = Processor(ages, vouchers, state, prices)
	val res = p.dfs()
	println("dfs result: ${res.price / 100f}€: ${res.toBuy}")
	return res
}

val baby = 3
val child = 16
val adult = 17

class SchwimmbadTest
{
	@Test(expectedExceptions = arrayOf(IllegalStateException::class))
	fun babies()
	{
		dfs(listOf(baby), 0, State(true, true))
		assert(false) // this code should not be executed, an exception should be thrown before
	}
	
	@Test
	fun aloneChildWeekend()
	{
		val res = dfs(listOf(child), 0, State(holidays = false, weekend = true))
		assertThat(res.price, equalTo(2 EUR 50))
	}
	@Test
	fun aloneChildWeekday()
	{
		val res = dfs(listOf(child), 0, State(holidays = false, weekend = false))
		assertThat(res.price, equalTo(2 EUR 0))
	}
	
	@Test
	fun aloneAdultWeekend()
	{
		val res = dfs(listOf(adult), 0, State(holidays = false, weekend = true))
		assertThat(res.price, equalTo(3 EUR 50))
	}
	@Test
	fun aloneAdultWeekday()
	{
		val res = dfs(listOf(adult), 0, State(holidays = false, weekend = false))
		assertThat(res.price, equalTo(2 EUR 80))
	}
	
	@Test
	fun adultWithVoucher()
	{
		val res = dfs(listOf(adult), 1, State(holidays = false, weekend = true))
		assertThat(res.price, equalTo(0))
	}
	@Test
	fun childWithVoucher()
	{
		val res = dfs(listOf(child), 1, State(holidays = false, weekend = true))
		assertThat(res.price, equalTo(0))
	}
	
	@Test
	fun adultsGroupSmall()
	{
		val res = dfs(collectionOf(4, { adult }), 0, State(holidays = false, weekend = false))
		assertThat(res.price, equalTo(11 EUR 0))
	}
	@Test
	fun adultsGroupLarge()
	{
		val res = dfs(collectionOf(6, { adult }), 0, State(holidays = false, weekend = false))
		assertThat(res.price, equalTo(11 EUR 0))
	}
	@Test
	fun childrenGroupSmall()
	{
		val res = dfs(collectionOf(5, { child }), 0, State(holidays = false, weekend = false))
		assertThat(res.price, equalTo(5 * (2 EUR 0)))
	}
	@Test
	fun childrenGroupLarge()
	{
		val res = dfs(collectionOf(6, { child }), 0, State(holidays = false, weekend = false))
		assertThat(res.price, equalTo(11 EUR 0))
	}
	
	@Test
	fun adultsGroupSmallVoucher()
	{
		val res = dfs(collectionOf(4, { adult }), 1, State(holidays = false, weekend = false))
		assertThat(res.price, equalTo(3 * (2 EUR 80)))
	}
	@Test
	fun adultsGroupLargeVoucher()
	{
		val res = dfs(collectionOf(6, { adult }), 1, State(holidays = false, weekend = false))
		assertThat(res.price, equalTo(9 EUR 90))
	}
	@Test
	fun childrenGroupVoucher()
	{
		val res = dfs(collectionOf(6, { child }), 1, State(holidays = false, weekend = false))
		assertThat(res.price, equalTo(9 EUR 90))
	}
	
	@Test
	fun groupWeekend()
	{
		val res = dfs(collectionOf(6, { adult }), 0, State(holidays = false, weekend = true))
		assertThat(res.price, equalTo(6 * (3 EUR 50)))
	}
	
	@Test
	fun twoGroups()
	{
		val res = dfs(collectionOf(12, { adult }), 0, State(holidays = false, weekend = false))
		assertThat(res.price, equalTo(2 * (11 EUR 0)))
	}
	
	@Test
	fun family1()
	{
		val res = dfs(listOf(adult, child, child, child), 0, State(holidays = false, weekend = true))
		assertThat(res.price, equalTo(8 EUR 0))
	}
	@Test
	fun family2()
	{
		val res = dfs(listOf(adult, adult, child, child), 0, State(holidays = false, weekend = true))
		assertThat(res.price, equalTo(8 EUR 0))
	}
	@Test
	fun noFamily()
	{
		val res = dfs(collectionOf(4, { child }), 0, State(holidays = false, weekend = true))
		assertThat(res.price, equalTo(4 * (2 EUR 50)))
	}
	
	@Test
	fun twoVouchers1()
	{
		val res = dfs(collectionOf(7, { adult }), 2, State(holidays = false, weekend = false))
		assertThat(res.price, equalTo(9 EUR 90))
	}
	@Test
	fun twoVouchers2()
	{
		val res = dfs(collectionOf(20, { adult }), 2, State(holidays = false, weekend = true))
		assertThat(res.price, equalTo(59 EUR 85))
	}
}
