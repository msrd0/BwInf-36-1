#!/bin/bash
set -e

cd "`dirname "$0"`"

# remove old build files
./gradlew clean

dir=`mktemp -d einsendung-XXXXXX`
mkdir -p $dir

# a list of all tasks
tasks="AutoScrabble Schwimmbad Zimmerbelegung"

# write the sources to the archive
cp -r res/ gradle/ gradlew{,.bat} build.gradle.kts settings.gradle $dir
for task in $tasks
do
	cp -r $task $dir
	test ! -d $dir/$task/out || rm -rf $dir/$task/out
done

# build everything
./gradlew assemble

# extract all the dist tar files
for task in $tasks
do
	tar xf $task/build/distributions/$task.tar -C $dir
done

# generate and add all the documentation
for task in $tasks
do
	./gendoc.sh "$task/README.md" "$task.pdf"
	cp "$task.pdf" $dir
done

# remove the readme files from the archive
rm $dir/{AutoScrabble,Schwimmbad,Zimmerbelegung}/README.md{,.html}

# create a zip file
pushd $dir
test ! -r "../einsendung.zip" || rm -f "../einsendung.zip"
zip -r "../einsendung.zip" .
popd

# remove our tmpdir
rm -rf $dir
