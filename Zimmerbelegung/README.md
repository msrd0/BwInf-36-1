# Zimmerbelegung

| Zimmer 1 | Zimmer 2 | Zimmer 3 |
| :------: | :------: | :------: |
| Lara     | Emma     | Alina    |
|          | Mia      | Lilli    |
|          | Zoe      |          |

## Lösungsidee

Gefordert ist, aus einer Liste von Schülern, von denen
jeder eine Liste von Freunden und Feinden hat, eine
optimale Zimmerbelegung zu erstellen, bei der jeder
Schüler mit jedem seiner Freunde und keinem seiner
Feinde zusammen im Zimmer ist, bzw. zu erkennen, dass
es keine optimale Zimmerbelegung gibt.

Ich betrachte die Schüler nun der Reihe nach. Zunächst
suche ich alle Freunde des Schülers und sorge dafür,
dass ich mich entweder gerade um diesen kümmere oder
diesem einen Raum zuweise. Wenn ein Schüler keine Freunde
hat oder ich mich um alle Freunde bereits kümmere, dann
landet dieser in einem neuen Raum. Andernfalls muss
der Schüler mit allen Freunden, die bereits einem Raum
zugewiesen wurden, in einen Raum. Sollten seine Freunde
in mehr als einem Raum verteilt sein, müssen diese Räume
fusioniert werden. Wenn ich davon ausgehe, dass ich Schüler
immer nur genau dann in denselben Raum tue, wenn sie in
irgendeiner Art und Weise miteinander befreundet sind,
kann ich davon ausgehen, dass in dem Fall einer nicht
funktionierenden Fusionierung keine optimale Zimmerbelegung
erstellt werden kann. Für den Fall, dass in dem Raum, dem
der Schüler hinzugefügt werden soll, ein Feind des Schülers
ist oder einer der Schüler im Raum den hinzuzufügenden
Schüler als Feind angegeben hat, ist es ebenfalls nicht
möglich, eine optimale Zimmerbelegung zu erstellen. Das
gilt natürlich auch dann, wenn ein Schüler sich selber als
Feind angibt, wobei man dann darüber nachdenken sollte,
den Schüler in den Raum namens _Psychatrie_ zu stecken ...

## Umsetzung

Ich habe die Lösungsidee in Kotlin umgesetzt. Ein Schüler
wird dabei durch die Klasse `Student` repräsentiert, die
den Namen, die Freundes- und Feindesliste des
Schülers sowie den zugeteilten Raum, oder `null` wenn der
Schüler noch keinem Raum zugeteilt wurde, speichert:

```kotlin
data class Student(
		val name : String
)
{
	val friends = HashSet<String>()
	val enemies = HashSet<String>()
	
	var room : Room? = null
}
```

Ein Raum hingegen speichert eine ID, die hinterher zur
Ausgabe benutzt wird, um die Räume zu unterschieden, sowie
eine Liste mit allen Mitgliedern des Raumes:

```kotlin
class Room
{
	val id = nextId;
	
	val members = HashSet<Student>()
```

Zudem bietet die Klasse `Room` zwei Funktionen, die
überprüfen, ob ein Schüler dem Raum hinzugefügt werden
kann oder zwei Räume fusioniert werden können:

```kotlin
fun canAdd(student : Student) : Boolean
{
	for (member in members)
		if (member.enemies.contains(student.name) ||
				student.enemies.contains(member.name))
			return false
	return true
}

fun canMerge(room : Room) : Boolean
{
	for (member in room.members)
		if (!canAdd(member))
			return false
	return true
}
```

Der eigentliche Algorithmus befindet sich in der Klasse
`Processor`. Diese nimmt als Parameter einen
`BufferedReader` mit dem Inhalt der Eingabedatei, der
im Konstruktor gelesen wird:

```kotlin
init
{
	var curr : Student? = null
	var line : String
	while (true)
	{
		line = file.readLine()?.trim() ?: break
		if (line.isEmpty())
			continue
		
		when (line[0])
		{
			'+'  -> curr!!.friends.addAll(line.substring(1)
						.trim()
						.split(whitespaces)
						.map { it.trim() }
						.filter { it.isNotEmpty() })
			'-'  -> curr!!.enemies.addAll(line.substring(1)
						.trim()
						.split(whitespaces)
						.map { it.trim() }
						.filter { it.isNotEmpty() })
			else ->
			{
				if (curr != null)
					students[curr.name] = curr
				curr = Student(line)
			}
		}
	}
	if (curr != null)
		students[curr.name] = curr
	file.close()
}
```

Die Methode `process` verarbeitet diese Daten anschließend
wie in der Lösungsidee beschrieben. Dafür ruft sie für
jeden Schüler der noch keinem Raum zugeteilt worden ist
(und der sich nicht selbst hasst) die Helferfunktion
`processStudent` auf:

```kotlin
fun process() : Processor
{
	for (student in students.values)
	{
		if (student.enemies.contains(student.name))
			throw ImpossibleException()
		
		if (student.room == null)
			processStudent(student)
	}
	
	return this
}
```

Die Helferfunktion `processStudent` versucht, einen Schüler
einem Raum zuzuweisen. Dafür nimmt sie den Schüler sowie
eine anfangs leere Liste mit Schülern entgegen, um die sich
bereits gekümmert wird. Durch diese Liste wird eine endlose
Rekursion verhindert, die entsteht, wenn sich zwei Schüler
gegenseitig auf die Freundesliste setzen (siehe Lösungsidee).


```kotlin
private fun processStudent(
		student : Student,
		notCareAbout : HashSet<String> = HashSet<String>()
)
{
	notCareAbout.add(student.name)
```

Zunächst sammelt die Methode eine Liste von Räumen, in denen
Freunde des Schülers sind, und ruft sich rekursiv für Freunde
auf, die noch keinem Raum zugeteilt worden sind:

```kotlin
	val suggestedRooms = ArrayList<Room>()
	for (friend in student.friends.filter {
		!notCareAbout.contains(it)
	}.map {
		students[it] ?: throw IllegalStateException("Missing student $it")
	})
	{
		if (friend.room == null)
			processStudent(friend, notCareAbout)
		if (!suggestedRooms.contains(friend.room!!))
			suggestedRooms.add(friend.room!!)
	}
```

Sollten dabei mehrere Räume in der Liste sein, müssen diese
fusioniert werden:

```kotlin
	while (suggestedRooms.size > 1)
	{
		if (!suggestedRooms[0].canMerge(suggestedRooms[1]))
			throw ImpossibleException()
		
		suggestedRooms[0].members.addAll(suggestedRooms[1].members)
		for (member in suggestedRooms[1].members)
			member.room = suggestedRooms[0]
		suggestedRooms.removeAt(1)
	}
```

Ist danach genau ein Raum in der Liste, muss der Schüler
diesem Raum hinzugefügt werden:

```kotlin
	if (suggestedRooms.size == 1)
	{
		if (suggestedRooms[0].canAdd(student))
		{
			suggestedRooms[0].members.add(student)
			student.room = suggestedRooms[0]
		}
		else
			throw ImpossibleException()
	}
```

Ansonsten erstellen wir einen neuen, leeren Raum und fügen
diesem den Schüler hinzu:

```kotlin
	else
	{
		student.room = Room()
		student.room!!.members.add(student)
	}
	
	notCareAbout.remove(student.name)
}
```

## Beispiele

Hier die Beispiele aus der Aufgabenstellung:

**zimmerbelegung1.txt**

```
Sorry, aber es können nicht alle Wünsche erfüllt werden
```

**zimmerbelegung2.txt**

```
Lara ist in Zimmer 1
Zoe ist in Zimmer 2
Mia ist in Zimmer 2
Alina ist in Zimmer 3
Lilli ist in Zimmer 3
Emma ist in Zimmer 2
```

**zimmerbelegung3.txt**

```
Michelle ist in Zimmer 1
Charlotte ist in Zimmer 5
Jasmin ist in Zimmer 5
Clara ist in Zimmer 7
Sofia ist in Zimmer 1
Luisa ist in Zimmer 5
Lilli ist in Zimmer 13
Lisa ist in Zimmer 1
Melina ist in Zimmer 13
Emily ist in Zimmer 1
Sarah ist in Zimmer 13
Katharina ist in Zimmer 13
Jessika ist in Zimmer 5
Josephine ist in Zimmer 13
Johanna ist in Zimmer 1
Anna ist in Zimmer 1
Merle ist in Zimmer 5
Lara ist in Zimmer 5
Celine ist in Zimmer 7
Sophie ist in Zimmer 13
Larissa ist in Zimmer 1
Celina ist in Zimmer 5
Vanessa ist in Zimmer 13
Pauline ist in Zimmer 13
Hannah ist in Zimmer 15
Leonie ist in Zimmer 13
Lena ist in Zimmer 7
Lea ist in Zimmer 7
Jana ist in Zimmer 1
Pia ist in Zimmer 13
Nina ist in Zimmer 5
Kim ist in Zimmer 13
Marie ist in Zimmer 1
Julia ist in Zimmer 1
Annika ist in Zimmer 13
Laura ist in Zimmer 5
Nele ist in Zimmer 1
Lina ist in Zimmer 7
Carolin ist in Zimmer 1
Alina ist in Zimmer 13
Emma ist in Zimmer 1
Miriam ist in Zimmer 5
Antonia ist in Zimmer 1
```

**zimmerbelegung4.txt**

```
Michelle ist in Zimmer 1
Charlotte ist in Zimmer 1
Jasmin ist in Zimmer 3
Clara ist in Zimmer 1
Sofia ist in Zimmer 7
Luisa ist in Zimmer 1
Lilli ist in Zimmer 7
Lisa ist in Zimmer 1
Melina ist in Zimmer 7
Emily ist in Zimmer 7
Sarah ist in Zimmer 7
Katharina ist in Zimmer 7
Jessika ist in Zimmer 3
Josephine ist in Zimmer 7
Johanna ist in Zimmer 7
Anna ist in Zimmer 3
Merle ist in Zimmer 1
Lara ist in Zimmer 1
Celine ist in Zimmer 1
Sophie ist in Zimmer 7
Larissa ist in Zimmer 7
Celina ist in Zimmer 1
Vanessa ist in Zimmer 1
Pauline ist in Zimmer 3
Hannah ist in Zimmer 1
Leonie ist in Zimmer 7
Lena ist in Zimmer 7
Lea ist in Zimmer 7
Jana ist in Zimmer 7
Pia ist in Zimmer 1
Nina ist in Zimmer 1
Kim ist in Zimmer 1
Marie ist in Zimmer 7
Julia ist in Zimmer 3
Annika ist in Zimmer 1
Laura ist in Zimmer 7
Nele ist in Zimmer 1
Lina ist in Zimmer 1
Carolin ist in Zimmer 1
Alina ist in Zimmer 7
Emma ist in Zimmer 1
Miriam ist in Zimmer 3
Antonia ist in Zimmer 9
```

**zimmerbelegung5.txt**

```
Sorry, aber es können nicht alle Wünsche erfüllt werden
```

**zimmerbelegung6.txt**

```
Michelle ist in Zimmer 1
Charlotte ist in Zimmer 2
Jasmin ist in Zimmer 7
Clara ist in Zimmer 2
Sofia ist in Zimmer 7
Luisa ist in Zimmer 2
Lilli ist in Zimmer 11
Lisa ist in Zimmer 2
Melina ist in Zimmer 11
Emily ist in Zimmer 11
Sarah ist in Zimmer 11
Katharina ist in Zimmer 2
Jessika ist in Zimmer 2
Josephine ist in Zimmer 2
Johanna ist in Zimmer 11
Anna ist in Zimmer 2
Merle ist in Zimmer 7
Lara ist in Zimmer 2
Celine ist in Zimmer 11
Sophie ist in Zimmer 11
Larissa ist in Zimmer 11
Celina ist in Zimmer 12
Vanessa ist in Zimmer 11
Pauline ist in Zimmer 2
Hannah ist in Zimmer 11
Leonie ist in Zimmer 2
Lena ist in Zimmer 2
Lea ist in Zimmer 1
Jana ist in Zimmer 11
Pia ist in Zimmer 2
Nina ist in Zimmer 2
Kim ist in Zimmer 2
Marie ist in Zimmer 2
Julia ist in Zimmer 2
Annika ist in Zimmer 12
Laura ist in Zimmer 2
Nele ist in Zimmer 2
Lina ist in Zimmer 2
Carolin ist in Zimmer 2
Alina ist in Zimmer 2
Emma ist in Zimmer 2
Miriam ist in Zimmer 2
Antonia ist in Zimmer 2
```
