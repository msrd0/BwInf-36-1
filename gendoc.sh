#!/bin/bash
set -e

file=`realpath "$1"`
out=`realpath "$2"`
cd `dirname "$0"`
tmp="$file.html"
#tmp=`mktemp "/tmp/gendocXXXXXX.html"`

grip "$file" --export "$tmp"
sed -i "$tmp" -e 's,</head>,<style>\
body { min-width: 100px; }\
p { text-align: justify; }\
.readme .markdown-body { border: 0; padding: 0; }\
.boxed-group>h3 { display: none; }\
.preview-page { margin: 0; }\
.container { margin: 0; width: 100%; }\
.markdown-body { font-size: 10px; }\
</style></head>,'
node gendoc.js "$tmp" "$out"
#rm "$tmp"
