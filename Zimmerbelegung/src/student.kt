/*
 36. BwInf
 Copyright (C) 2017 Dominic Meiser
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
*/
package msrd0.bwinf36.zimmerbelegung

data class Student(
		val name : String
)
{
	/** Die Freunde des Schülers, die in demselben Raum sein sollten. */
	val friends = HashSet<String>()
	/** Die Feinde des Schülers, die nicht in demselben Raum sein sollten. */
	val enemies = HashSet<String>()
	
	/** Der Raum dem der Schüler zugeteilt wurde, oder null wenn der Schüler noch nicht eingeteilt wurde. */
	var room : Room? = null
}

class Room
{
	companion object
	{
		private var lastId = 0
		private val nextId get() = ++lastId
	}
	
	val id = nextId;
	override fun equals(other : Any?) : Boolean = other != null && other is Room && other.id == id
	override fun hashCode() = id
	override fun toString() = "Zimmer $id"
	
	/** Alle Schüler in diesem Raum. */
	val members = HashSet<Student>()
	
	/** Gibt true zurück wenn der Schüler diesem Raum zugeteilt werden kann. */
	fun canAdd(student : Student) : Boolean
	{
		// für jeden Schüler im Raum überprüfen dass sie sich gegenseitig vertragen
		for (member in members)
			if (member.enemies.contains(student.name) || student.enemies.contains(member.name))
				return false
		// wenn sich alle vertragen können wir den Schüler diesem Raum zuteilen
		return true
	}
	
	/** Gibt true zurück wenn die beiden Räume fusioniert werden können. */
	fun canMerge(room : Room) : Boolean
	{
		// überprüfen, dass wir jeden Schüler des anderen Raumes diesem Raum hinzufügen können
		for (member in room.members)
			if (!canAdd(member))
				return false
		// wenn alle Schüler diesem Raum hinzugefügt werden können, können die beiden Räume fusioniert werden
		return true
	}
}
