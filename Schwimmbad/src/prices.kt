/*
 36. BwInf
 Copyright (C) 2017 Dominic Meiser
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
*/
package msrd0.bwinf36.schwimmbad

/**
 * Diese Klasse speichert einen Status, ob Ferien und/oder Wochenende ist.
 */
data class State(
		val holidays : Boolean,
		val weekend : Boolean
)

/**
 * Dieses Interface beschreibt einen Preis.
 */
interface IPrice
{
	val name : String
	val price : Int
	
	val adults : Int
	val children : Int
	
	val childAsAdult : Boolean
	
	fun applies(state : State) : Boolean
}

/**
 * Diese Klasse implementiert [IPrice] durch Konstruktor-Parameter.
 */
data class Price(
		override val name : String,
		override val price : Int,
		override val adults : Int,
		override val children : Int,
		override val childAsAdult : Boolean,
		val duringHolidays : Boolean = true,
		val duringSchooldays : Boolean = true,
		val duringWeekend : Boolean = true,
		val duringWeekdays : Boolean = true
) : IPrice
{
	override fun applies(state : State) : Boolean
			= (if (state.holidays) duringHolidays else duringSchooldays)
			&& (if (state.weekend) duringWeekend else duringWeekdays)
	
	override fun toString() = "Price('$name': ${price/100f}€ for $adults+$children)"
}

typealias PriceList = Collection<IPrice>

fun PriceList.format() : String
{
	val map = HashMap<String, Int>()
	forEach { map[it.name] = (map[it.name] ?: 0) + 1 }
	return map.entries.joinToString { (key, value) -> "${value}x $key" }
}

fun defaultPrices() : PriceList = listOf(
		Price("Einzelticket", 3 EUR 50, 1, 0, false, duringWeekdays = false),
		Price("Einzelticket Ermäßigt", 2 EUR 50, 0, 1, false, duringWeekdays = false),
		Price("Einzelticket", 2 EUR 80, 1, 0, false, duringWeekend = false),
		Price("Einzelticket Ermäßigt", 2 EUR 0, 0, 1, false, duringWeekend = false),
		Price("Tageskarte", 11 EUR 0, 6, 0, true, duringWeekend = false),
		Price("Familienkarte", 8 EUR 0, 2, 2, false),
		Price("Familienkarte", 8 EUR 0, 1, 3, false)
)

val PriceList.maximumGroupSize get() = map { it.adults + it.children }.sortedDescending().first()
