/*
 36. BwInf
 Copyright (C) 2017 Dominic Meiser
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
*/
package msrd0.bwinf36.autoscrabble

import java.io.*
import java.util.*

data class NumberPlate(
		val city : String,
		val text : String,
		val prev : NumberPlate? = null
) : Iterable<NumberPlate>
{
	companion object
	{
		val cities = HashSet<String>()
		private val rng = Random()
		private val randInt get() = rng.nextInt(9999) + 1
		
		fun loadCities()
		{
			val file = BufferedReader(InputStreamReader(ClassLoader.getSystemResourceAsStream("kuerzelliste.txt")))
			while (true)
			{
				val line = file.readLine()?.trim() ?: break
				if (line.isNotEmpty())
					cities.add(line)
			}
			file.close()
		}
		
		val textRegex = Regex("[A-Z]{1,2}")
	}
	
	init
	{
		if (!textRegex.matches(text))
			throw IllegalArgumentException("Illegal value for text: $text")
	}
	
	override fun toString() = "$city-$text $randInt"
	
	override fun iterator() : Iterator<NumberPlate>
	{
		val list = LinkedList<NumberPlate>()
		var next : NumberPlate? = this
		while (next != null)
		{
			list.offerFirst(next)
			next = next.prev
		}
		return list.iterator()
	}
}
