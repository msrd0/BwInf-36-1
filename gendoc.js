var fs = require('fs');
var pdf = require('html-pdf');
var html = fs.readFileSync(process.argv[2], 'utf8');

var options = {
	"format": "A4",
	"border": "2cm",
	
	"header": {
		"height": "2cm",
		"contents": '<table style="font-size:0.8em" width="100%"><tr><td align="left">Dominic Meiser</td><td align="center">36. BwInf Runde 1</td><td align="right">Teilnahme-ID: 44762</td></tr></table>'
	},
	"footer": {
		"height": "2cm",
		"contents": '<div style="text-align:center;margin-top:1.5cm;font-size:0.8em">Seite {{page}} von {{pages}}</div>'
	}
};

pdf.create(html, options).toFile(process.argv[3], function(err, res) {
	if (err)
		return console.log(err);
	console.log(res);
});
