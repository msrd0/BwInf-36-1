/*
 36. BwInf
 Copyright (C) 2017 Dominic Meiser
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
*/
@file:JvmName("Main")
package msrd0.bwinf36.autoscrabble

import msrd0.bwinf36.autoscrabble.NumberPlate.Companion.cities
import org.slf4j.*
import java.io.*
import java.nio.file.*
import java.util.*

private val logger : Logger = LoggerFactory.getLogger("msrd0.bwinf36.autoscrabble.Main")


/**
 * Diese Klasse dient als Prozessor eines Strings, der in ein Kennzeichen umgewandelt werden muss. Der Konstruktor
 * nimmt als einziges Argument diesen String entgegen.
 */
class Processor(val str : String)
{
	/**
	 * Die Queue (Deque, double-ended Queue), die ein Paar aus dem Index in [str] und der letzten [NumberPlate]
	 * speichert.
	 */
	val q : Deque<Pair<Int, NumberPlate?>> = LinkedList()
	/**
	 * Ein Set aus bereits angeschauten Indizes in [str], die "verbrannt" werden können.
	 */
	val burned = HashSet<Int>()
	
	init
	{
		// der Konstruktor setzt das erste Element in die Queue
		q.offerFirst(0 to null)
	}
	
	/** Die maximale Größe der Queue. Wird nur zu statistischen Zwecken benötigt. */
	var maxQ : Int = 0
		private set
	/** Die anzahl an "verbranten" Indizes. Wird nur zu statistischen Zwecken benötigt. */
	var numBurned : Int = 0
		private set
	
	/**
	 * Helferfunktion für [bfs]. Die Methode sucht nach moglichen Nummernschildern, die aus der letzten [NumberPlate]
	 * [e], der Anzahl verbleibenden Zeichen [remaining] und der Länge des Stadtkürzels [cityLen] entstehen können.
	 * Wenn dabei eine gültige Kennzeichenfolge entsteht, gibt die Funktion diese zurück, andernfalls wird alles der
	 * Queue hinzugefügt.
	 */
	private fun bfs0(e : Pair<Int, NumberPlate?>, remaining : Int, cityLen : Int) : Iterable<NumberPlate>?
	{
		// die Anzahl an verbleibenden Zeichen muss größer sein als die Länge des Stadtkürzels
		if (remaining <= cityLen)
			return null
		// der Index in [str] nach dem Stadtkürzel
		val afterCity = e.first + cityLen
		// das Stadtkürzel
		val city = str.substring(e.first, afterCity)
		// das Stadtkürzel muss in der Liste der erlaubten Stadtkürzel sein
		if (!cities.contains(city))
			return null
		
		// wenn nach dem Stadtkürzel noch maximal zwei Zeichen folgen, sind wir fertig
		if (remaining <= cityLen + 2 && NumberPlate.textRegex.matches(str.substring(afterCity)))
			return NumberPlate(city, str.substring(afterCity), e.second)
		
		// andernfalls überprüfen wir, ob eine mögliche Kennzeichenfolge schon einmal bei demselben Index ausgekommen
		// ist, und fügen es entsprechend der Queue hinzu. Einmal für 2 Zeichen ...
		if (!burned.contains(afterCity + 2) && NumberPlate.textRegex.matches(str.substring(afterCity, afterCity + 2)))
		{
			q.offerLast(afterCity + 2 to
					NumberPlate(city, str.substring(afterCity, afterCity + 2), e.second))
			burned.add(afterCity + 2)
		}
		else
			numBurned++
		
		// ... und einmal für ein Zeichen
		if (!burned.contains(afterCity + 1) && NumberPlate.textRegex.matches(str.substring(afterCity, afterCity + 1)))
		{
			q.offerLast(afterCity + 1 to
					NumberPlate(city, str.substring(afterCity, afterCity + 1), e.second))
			burned.add(afterCity + 1)
		}
		else
			numBurned++
		
		return null
	}
	
	/**
	 * Diese Methode sucht mithilfe einer Breitensuche nach einer möglichen Kennzeichenfolge, die [str] nachbilden.
	 */
	fun bfs() : Iterable<NumberPlate>?
	{
		// solange weitersuchen, bis die Queue leer ist
		while (q.isNotEmpty())
		{
			maxQ = q.size
			val e = q.removeFirst()
			val remaining = str.length - e.first
			
			// nach Kennzeichen suchen, deren Städtekürzel die Länge 3 hat ...
			var res = bfs0(e, remaining, 3)
			if (res != null)
				return res
			
			// ... oder 2 hat ...
			res = bfs0(e, remaining, 2)
			if (res != null)
				return res
			
			// ... oder 1 hat
			res = bfs0(e, remaining, 1)
			if (res != null)
				return res
		}
		
		// die Suche war leider nicht erfolgreich
		return null
	}
	
}

fun main(args : Array<String>)
{
	// als Erstes müssen alle Städtekürzel geladen werden
	logger.info("Loading cities")
	NumberPlate.loadCities()
	
	// danach können alle Strings aus der Eingabedatei verarbeitet werden
	val file = BufferedReader(FileReader(args[0]))
	var line : String
	while (true)
	{
		line = file.readLine()?.trim() ?: break
		if (line.isEmpty())
			continue
		
		val p = Processor(line)
		val res = p.bfs()
		logger.debug("Die maximale Größe der Queue betrug ${p.maxQ} für ${p.str}. Es wurden ${p.numBurned} Möglichkeiten übersprungen")
		if (res == null)
			logger.info("$line kann nicht dargestellt werden")
		else
			logger.info("aus $line wird  ${res.joinToString("  ")}")
	}
	file.close()
}
