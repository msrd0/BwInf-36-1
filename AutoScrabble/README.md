# AutoScrabble (Aufgabe 4)

ZW-EI 1454  BI-L 6712  LI-ON 511  EN-EI 9131  NH-UN 4447  DE-RT 4416  F-UE 3305  NF-UN 1412  DZ-WA 4704  NZ-IG 5028  MIL-LI 9898  A-RD 7330  EN-EI 7725  NH-UN 9777  DE-RT 6913  F-UE 1868  NF-UN 9911  DZ-WA 1230  NZ-IG 2572  MI-L 2626  LI-ON 3086  EN-EI 6438  NH-UN 1683  DE-RT 4215  A-C 6249  H-TT 6892  AU-SE 7527  ND-ZW 8646  EI-HU 3258  ND-E 14  RT-ZW 4780  EI-UN 8083  DD-R 811  EIS-SI 4379  GE-U 4614  R-O 8747

## Lösungsidee

Gefordert ist, aus einem beliebig langen Wort ein oder
mehrere Kennzeichen zu erzeugen, deren Buchstaben das
Wort bilden. Die Zahlen auf den Kennzeichen spielen keine
Rolle und können somit vernachlässigt werden. Die
Buchstaben müssen jedoch bestimmten Regeln folgen. So
müssen die ersten ein, zwei oder drei Buchstaben in
einer Liste enthalten sein, gefolgt von ein oder zwei
Buchstaben, die keine Umlaute sein dürfen.

Dies kann mit einer simplen Breitensuche implementiert
werden. Da nicht nach allen Möglichkeiten, sondern nur
nach einer gefragt ist, können wir die Knoten als Index
im Wort ansehen. Da Breitensuche nur auf Bäumen
funktioniert (ansonsten könnte sie sich im Kreis drehen),
müssen wir alle doppelten Wege entfernen.
Diese entstehen, da man beispielsweise die Zeichenfolge
`AAA` sowohl als `AA-A` als auch als `A-AA` schreiben
kann. Es ist hierbei egal, für welchen Weg man sich
entscheidet, aber um im Graphen die Eigenschaften eines
Baumes aufrecht zu erhalten, darf immer nur ein Weg
gespeichert werden.

Der Algorithmus beginnt mit dem Index 0, also dem ersten
Zeichen im Wort. Es können jetzt maximal 6 Nummernschilder
gebildet werden, mit einem Städtekürzel von ein bis drei
Zeichen und einem Mittelteil von ein bis zwei Zeichen.
Dabei muss immer sichergestellt werden, dass das
Städtekürzel in der Liste der verfügbaren Städtekürzel
ist. Anschließend werden maximal vier Möglichkeiten der
Queue hinzugefügt, von einer minimalen Länge von 2 Zeichen
bis zu einer maximalen Länge von 5 Zeichen auf dem
Kennzeichen. Für jedes Element in der Queue wird dies
wiederholt, bis irgendwann entweder eine Kennzeichenfolge
gefunden wurde, dies das Wort exakt wiedergibt, oder
die Queue leer ist, was bedeutet, dass es keine
Kennzeichenfolge gibt, die das Wort exakt beinhaltet.

Durch diesen Algorithmus wird zudem sichergestellt, dass
immer die kürzest mögliche Kennzeichenfolge zurückgegeben
wird. Dies ist allerdings nicht von der Aufgabenstellung
gefordert, aber es dürfte Heike helfen, ihr Wort schneller
zu finden, da sie dafür weniger Autos braucht.

## Umsetzung

Ich habe die Lösungsidee in Kotlin umgesetzt. Ein
Nummernschild wird durch die Klasse `NumberPlate`
repräsentiert, welche zum einen das Städtekürzel und
den Mittelteil, und zum anderen eine Referenz auf das
vorhergehende Kennzeichen oder `null` enthält:

```kotlin
data class NumberPlate(
		val city : String,
		val text : String,
		val prev : NumberPlate? = null
) : Iterable<NumberPlate>
```

Der letzte Teil eines Kennzeichens, die Zahl, muss jedoch
nicht gespeichert werden, sondern kann einfach zufällig
generiert werden:

```kotlin
private val randInt get() = rng.nextInt(9999) + 1
override fun toString() = "$city-$text $randInt"
```

Der eigentliche Algorithmus befindet sich in der Klasse
`Processor`. Diese speichert das Wort sowie Queue aus
Index und Kennzeichenfolge, die mit dem Index 0 und einer
leeren Kennzeichenfolge initialisiert wird, sowie eine
Liste an bereits besuchten Indizes:

```kotlin
class Processor(val str : String)
{
	val q : Deque<Pair<Int, NumberPlate?>> = LinkedList()
	val burned = HashSet<Int>()
	
	init
	{
		q.offerFirst(0 to null)
	}
```

Die `bfs`-Methode sucht anschließend nach einer möglichen
Kennzeichenfolge, wie in der Lösungsidee beschrieben:

```kotlin
fun bfs() : Iterable<NumberPlate>?
{
```

Die Methode sucht die Queue ab bis sie leer ist oder eine
Lösung gefunden wurde


```kotlin
	while (q.isNotEmpty())
	{
		maxQ = q.size
		val e = q.removeFirst()
		val remaining = str.length - e.first
```

Für Städtekürzel mit drei, zwei oder einem Zeichen wird
jeweils die Helferfunktion `bfs0` aufgerufen. Sollte diese
ein Ergebnis liefern, wird dieses zurückgegeben.
Ansonsten fügt `bfs0` automatisch die gefundenen
Möglichkeiten der Queue hinzu.

```kotlin
		var res = bfs0(e, remaining, 3)
		if (res != null)
			return res
		
		res = bfs0(e, remaining, 2)
		if (res != null)
			return res
		
		res = bfs0(e, remaining, 1)
		if (res != null)
			return res
	}
```

Sollte die Queue leer sein, ohne das vorher eine Lösung
gefunden und zurückgegeben wurde, gibt es keine Lösung
und es wird `null` zurückgegeben.

```kotlin
	return null
}
```

Die Helferfunktion `bfs0` wiederum sucht bei angegebenem
Index, Anzahl an verbleibenden Zeichen und Länge des
Städtekürzels nach der/den nächsten möglichen
Nummernschildern.

```kotlin
private fun bfs0(
		e : Pair<Int, NumberPlate?>,
		remaining : Int,
		cityLen : Int
) : Iterable<NumberPlate>?
{
```

Dafür muss die Anzahl an verbleibenden Zeichen größer sein
als die Länge des Städtekürzels:

```kotlin
	if (remaining <= cityLen)
		return null
```

Und das Städtekürzel muss in der Liste der erlaubten
Städtekürzel enthalten sein:

```kotlin
	val afterCity = e.first + cityLen
	val city = str.substring(e.first, afterCity)
	if (!cities.contains(city))
		return null
```

Wenn nach dem Stadtkürzel noch maximal zwei Zeichen
folgen (keine Umlaute), haben wir eine Lösung gefunden:

```kotlin
	if (remaining <= cityLen + 2 &&
			NumberPlate.textRegex.matches(str.substring(afterCity)))
		return NumberPlate(city, str.substring(afterCity), e.second)
```

Andernfalls wird überprüft, ob eine mögliche
Kennzeichenfolge schon einmal bei demselben Index
ausgekommen ist, und fügen sie ansonsten der Queue hinzu;
einmal für einen Mittelteil mit einem oder mit zwei
Zeichen. Auch hier darf der Mittelteil keine Umlaute
enthalten.

```kotlin
	if (!burned.contains(afterCity + 2) &&
			NumberPlate.textRegex.matches(str.substring(
				afterCity, afterCity + 2)))
	{
		q.offerLast(afterCity + 2 to
				NumberPlate(city, str.substring(afterCity,
					afterCity + 2), e.second))
		burned.add(afterCity + 2)
	}
	else
		numBurned++
	
	if (!burned.contains(afterCity + 1) &&
			NumberPlate.textRegex.matches(str.substring(
				afterCity, afterCity + 1)))
	{
		q.offerLast(afterCity + 1 to
				NumberPlate(city, str.substring(afterCity,
					afterCity + 1), e.second))
		burned.add(afterCity + 1)
	}
	else
		numBurned++
```

Da wir noch keine fertige Lösung gefunden haben, wird null
zurückgegeben, damit `bfs` die Queue weiter abarbeitet.

```kotlin
	return null
}
```

## Beispiele

Hier ein paar Beispiele ...

### aus der Aufgabenstellung

**BIBER**

BI-B 6549  E-R 3586

**BUNDESWETTBEWERB**

B-UN 4396  DE-SW 1135  E-TT 8477  BE-WE 4547  R-B 3078

**CLINTON**

C-LI 2397  NT-ON 2039

**DONAUDAMPFSCHIFFFAHRTSKAPITÄNSMÜTZE**

Keine Lösung

**ETHERNET**

E-TH 6581  ER-N 8549  E-T 9050

**INFORMATIK**

IN-FO 7338  R-M 3229  AT-IK 645

**LLANFAIRPWLLGWYNGYLLGOGERYCHWYRNDROBWLLLLANTYSILIOGOGOGOCH**

LL-AN 5730  F-AI 6651  RP-WL 2895  LG-WY 7203  N-GY 9324  LL-GO 7860  GER-YC 2834  H-WY 4785  RN-DR 1372  OB-WL 3516  LL-LA 906  NT-YS 7709  IL-IO 1412  G-OG 3945  OG-O 5836  C-H 8834

**RINDFLEISCHETIKETTIERUNGSÜBERWACHUNGSAUFGABENÜBERTRAGUNGSGESETZ**

Keine Lösung

**SOFTWARE**

SO-FT 2468  WA-RE 3249

**TRUMP**

TR-U 699  M-P 4409

**TSCHÜSS**

Keine Lösung

**VERKEHRSWEGEPLANUNGSBESCHLEUNIGUNGSGESETZ**

VER-KE 3770  HR-SW 4072  EG-EP 6107  LAN-UN 313  GS-BE 302  SC-HL 491  EU-NI 2147  GUN-GS 4780  GE-S 5548  E-TZ 7693

### von Wörtern, die nicht dargestellt werden können:

**TIMO**

Keine Lösung

**TIPP**

Keine Lösung

**TIM**

Keine Lösung

**OK**

Keine Lösung

### mit den Schulden Deutschlands

laut http://www.sprachlog.de/2013/06/05/das-neue-laengste-wort-des-deutschen/

**ZWEIBILLIONENEINHUNDERTFUENFUNDZWANZIGMILLIARDENEINHUNDERTFUENFUNDZWANZIGMILLIONENEINHUNDERTACHTTAUSENDZWEIHUNDERTZWEIUNDDREISSIGEURO**

ZW-EI 1454  BI-L 6712  LI-ON 511  EN-EI 9131  NH-UN 4447  DE-RT 4416  F-UE 3305  NF-UN 1412  DZ-WA 4704  NZ-IG 5028  MIL-LI 9898  A-RD 7330  EN-EI 7725  NH-UN 9777  DE-RT 6913  F-UE 1868  NF-UN 9911  DZ-WA 1230  NZ-IG 2572  MI-L 2626  LI-ON 3086  EN-EI 6438  NH-UN 1683  DE-RT 4215  A-C 6249  H-TT 6892  AU-SE 7527  ND-ZW 8646  EI-HU 3258  ND-E 14  RT-ZW 4780  EI-UN 8083  DD-R 811  EIS-SI 4379  GE-U 4614  R-O 8747
