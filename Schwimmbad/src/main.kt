/*
 36. BwInf
 Copyright (C) 2017 Dominic Meiser
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
*/
@file:JvmName("Main")
package msrd0.bwinf36.schwimmbad

import java.util.*
import kotlin.io.readLine as kotlinReadLine

/**
 * Diese Klasse speichert das Ergebnis: Den Gesamtpreis sowie alle Tickets.
 */
data class Result(
		val price : Int,
		val toBuy : PriceList
)

/**
 * Diese Klasse enthält den eigentlichen Algorithmus. Der Konstruktor nimmt ein
 * [Iterable] mit dem Alter aller Personen, die Anzahl an Gutscheinen, den Status
 * sowie die Preisliste des Schwimmbads entgegen.
 */
class Processor(
		val ages : Iterable<Int>,
		val vouchers : Int,
		val state : State,
		priceList : PriceList
)
{
	/** Die Anzahl an Babies. */
	val babies : Int
	/** Die Anzahl an Kindern. */
	val children : Int
	/** Die Anzahl an Erwachsenen. */
	val adults : Int
	/** Die Preisliste des Schwimmbads. */
	var priceList : PriceList
			private set
	
	init
	{
		this.priceList = priceList.filter { it.applies(state) }
		
		babies = ages.filter { it < 4 }.size
		children = ages.filter { it in 4..16 }.size
		adults = ages.filter { it > 16 }.size
	}
	
	/**
	 * Diese Klasse speichert die verbleibende Anzahl an Kindern, Erwachsenen und Gutscheinen.
	 * Sie ist der Schlüssel für [dp].
	 */
	private data class Remaining(
			val adults : Int,
			val children : Int,
			val vouchers : Int)
	{
		fun minus(a : Int = 0, c : Int = 0, v : Int = 0) : Remaining
			= Remaining(adults-a, children-c, vouchers-v)
	}
	
	/**
	 * Diese Methode startet die Tiefensuche und gibt das Ergebniss zurück.
	 */
	fun dfs() : Result
	{
		if (babies > 0 && adults == 0)
			throw IllegalStateException("Babies müssen immer in Begleitung eines Erwachsenen sein")
		return dfs(Remaining(adults, children, if (state.holidays) 0 else vouchers))
	}
	
	/** Dynamic Programming */
	private val dp = HashMap<Remaining, Result>()
	
	/**
	 * Diese Methode enthält den eigentlichen Algorithmus und gibt das Ergebnis zurück.
	 */
	private fun dfs(r : Remaining) : Result
	{
		// wenn r schon besucht wurde, das Ergebnis zurückgeben
		if (dp.containsKey(r))
			return dp[r]!!
		
		// wenn keine Personen vorhanden sind, müssen wir auch nix bezahlen
		if (r.children == 0 && r.adults == 0)
			return Result(0, emptyList())
		
		// diese Variablen speichern die aktuell besten Tickets
		var bestPrice = Int.MAX_VALUE
		var bestBuy : Collection<IPrice> = emptyList()
		
		// wenn es noch einen Gutschein gibt, dann können wir mindestens einer Person
		// freien Eintritt geben
		if (r.vouchers > 0)
		{
			for (a in 0 .. r.vouchers) // über die Anzahl an Gutscheinen für Erwachsenen iterieren
			{
				if (a > r.adults)
					break
				
				for (c in 0 .. (r.vouchers - a)) // über die Annzahl an Gutscheinen für Kinder iterieren
				{
					if (a == 0 && c == 0)
						continue
					if (c > r.children)
						break
					
					val res = dfs(r.minus(a=a, c=c, v=a+c))
					if (res.price < bestPrice)
					{
						bestPrice = res.price
						bestBuy = res.toBuy
					}
				}
			}
		}
		
		// alle Tickets durchsuchen
		for (p in priceList)
		{
			for (a in 0 .. p.adults) // über die Anzahl an Erwachsenen iterieren
			{
				if (a > r.adults)
					break
				
				for (c in 0 .. (p.children + if (p.childAsAdult) (p.adults - a) else 0)) // über die Anzahl an Kindern iterieren
				{
					if (a == 0 && c == 0)
						continue
					if (c > r.children)
						break
					
					// rekursiv aufrufen, dabei die Anzahl an Erwachsenen/Kindern, die in diesem Ticket enthalten
					// sind, abziehen
					val res = dfs(r.minus(a=a, c=c, v=r.vouchers))
					var price = p.price + res.price
					// evtl. Gutschein anwenden
					if (r.vouchers > 0)
						price = ceil(price * 0.9)
					// wenn dieses Ticket billiger ist, die Lösung speichern
					if (price < bestPrice)
					{
						bestPrice = price
						bestBuy = LinkedList(res.toBuy)
						bestBuy.offerFirst(p)
					}
				}
			}
		}
		
		if (bestPrice == Int.MAX_VALUE)
			throw RuntimeException("Couldn't find any solution, check the price list!")
		
		// das Ergebnis in dp speichern und zurückgeben
		val res = Result(bestPrice, bestBuy)
		dp[r] = res
		return res
	}
}

private fun readLine() : String = kotlinReadLine()!!.trim()
private fun readLineBoolean() : Boolean
{
	val tmp = readLine()
	return (tmp.isEmpty() || tmp[0].toLowerCase() != 'n')
}

private fun readAges() : Iterable<Int>
{
	val line = readLine()
	val ages = LinkedList<Int>()
	line.split(Regex("\\s+")).forEach {
		if (it.contains("x"))
		{
			val s = it.split("x")
			val age = s[1].toInt()
			for (i in 0 until s[0].toInt())
				ages.offerLast(age)
		}
		else
			ages.offerLast(it.toInt())
	}
	return ages
}

fun main(args : Array<String>)
{
	print("Alter aller Personen: ")
	val ages = readAges()
	print("Anzahl an Gutscheinen: ")
	val vouchers = readLine().toInt()
	print("Ferien? [Y/n] ")
	val holidays = readLineBoolean()
	print("Wochenende? [Y/n] ")
	val weekend = readLineBoolean()
	
	val p = Processor(ages, vouchers, State(holidays, weekend), defaultPrices())
	val res = p.dfs()
	print("Die folgenden Tickets sollten gekauft werden: ")
	println(res.toBuy.format())
	println("Alles zusammen kostet ${res.price / 100f} €")
}
