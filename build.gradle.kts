import org.gradle.jvm.tasks.Jar
import org.jetbrains.kotlin.gradle.plugin.KotlinPlatformJvmPlugin

buildscript {
	repositories {
		mavenLocal()
		jcenter()
	}
	dependencies {
		classpath(kotlin("gradle-plugin"))
	}
}

val Project.mainClass
	get() = "msrd0.bwinf36.${project.name.toLowerCase()}.Main"
fun DependencyHandlerScope.compile(dependency : Any)
		= dependencies.add("compile", dependency)
fun DependencyHandlerScope.testCompile(dependency : Any)
		= dependencies.add("testCompile", dependency)

subprojects {
	
	apply<KotlinPlatformJvmPlugin>()
	
	apply<ApplicationPlugin>()
	configure<ApplicationPluginConvention> {
		mainClassName = project.mainClass
	}
	
	repositories {
		mavenLocal()
		jcenter()
	}
	
	dependencies {
		compile(kotlin("stdlib"))
		
		if (project.name != "Schwimmbad")
		{
			compile("org.slf4j:slf4j-api:1.7.25")
			compile("ch.qos.logback:logback-classic:1.2.3")
		}
		
		testCompile("org.testng:testng:6.11")
		testCompile("org.hamcrest:hamcrest-integration:1.3")
	}
	
	the<JavaPluginConvention>().sourceSets {
		"main" {
			java {
				srcDirs("src")
			}
			resources {
				srcDirs("res", "../res")
			}
		}
		"test" {
			java {
				srcDirs("test/src")
			}
			resources {
				srcDirs("test/res")
			}
		}
	}
	
	tasks.withType(Test::class.java) {
		useTestNG()
	}
	
	tasks.withType(Jar::class.java) {
		manifest.attributes.apply {
			put("Main-Class", project.mainClass)
			put("Name", "msrd0/bwinf36/${project.name.toLowerCase()}/")
			put("Author", "Dominic Meiser")
		}
	}
}
