/*
 36. BwInf
 Copyright (C) 2017 Dominic Meiser
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
*/
package msrd0.bwinf36.test

import msrd0.bwinf36.zimmerbelegung.*
import org.hamcrest.MatcherAssert.*
import org.slf4j.*
import org.testng.annotations.Test
import java.io.*

val logger : Logger = LoggerFactory.getLogger(ZimmerbelegungTest::class.java)

fun example(nr : Int)
{
	val p : Processor
	try
	{
		logger.info("Processing example $nr")
		p = Processor(BufferedReader(InputStreamReader(ClassLoader.getSystemResourceAsStream("zimmerbelegung$nr.txt")))).process()
		logger.info("Finished processing example $nr")
	}
	catch (ex : ImpossibleException)
	{
		logger.info(ex.message)
		return
	}
	
	// sicherstellen, dass jeder zufrieden ist
	for (student in p.students.values)
	{
		// der Schüler sollte einem Raum zugeteilt worden sein
		assertThat("Check that ${student.name} is in a room",
				student.room != null)
		val room = student.room!!
		
		// alle Freunde des Schülers sollten existieren und im selben Raum sein
		for (friend in student.friends)
		{
			assertThat("Check that $friend, friend of ${student.name}, exists in ${p.students.keys.joinToString(", ")}",
					p.students.containsKey(friend))
			assertThat("Check that $friend, friend of ${student.name}, is in the same room as ${room.members.joinToString(", ")}",
					room.members.contains(p.students[friend]))
		}
		
		// alle Feinde des Schülers sollten existieren aber nicht im gleichen Raum sein
		for (enemy in student.enemies)
		{
			assertThat("Check that $enemy, enemy of ${student.name}, exists in ${p.students.keys.joinToString(", ")}",
					p.students.containsKey(enemy))
			assertThat("Check that $enemy, enemy of ${student.name}, is not in the same room as ${room.members.joinToString(", ")}",
					!room.members.contains(p.students[enemy]))
		}
	}
	
	// bestanden
	logger.info("Example $nr was successful")
}

class ZimmerbelegungTest
{
	@Test fun example1() = example(1)
	@Test fun example2() = example(2)
	@Test fun example3() = example(3)
	@Test fun example4() = example(4)
	@Test fun example5() = example(5)
	@Test fun example6() = example(6)
}
