# BwInf-36-1 [![pipeline status](https://gitlab.com/msrd0/BwInf-36-1/badges/master/pipeline.svg)](https://gitlab.com/msrd0/BwInf-36-1/commits/master)

Meine Einsendung zum 36. Bundeswettbewerb Informatik Runde 1. Alle Aufgaben sind in Kotlin geschrieben und können mit Gradle ausgeführt werden.

[![Poster](https://www.bwinf.de/fileadmin/user_upload/squirrel.jpg)](https://www.bwinf.de/bundeswettbewerb/der-36-bwinf/)
