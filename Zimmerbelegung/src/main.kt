/*
 36. BwInf
 Copyright (C) 2017 Dominic Meiser
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/gpl-3.0>.
*/
@file:JvmName("Main")
package msrd0.bwinf36.zimmerbelegung

import org.slf4j.*
import java.io.*

private val logger : Logger = LoggerFactory.getLogger("msrd0.bwinf36.zimmerbelegung.Main")

val whitespaces = Regex("\\s+")


class ImpossibleException : Exception("Sorry, aber es können nicht alle Wünsche erfüllt werden")


class Processor(file : BufferedReader)
{
	/** Alle Schüler, sortiert nach deren Namen. */
	val students = HashMap<String, Student>()
	
	init
	{
		// alle Schüler einlesen
		var curr : Student? = null
		var line : String
		while (true)
		{
			line = file.readLine()?.trim() ?: break
			if (line.isEmpty())
				continue
			
			when (line[0])
			{
				'+'  -> curr!!.friends.addAll(line.substring(1).trim().split(whitespaces).map { it.trim() }.filter { it.isNotEmpty() })
				'-'  -> curr!!.enemies.addAll(line.substring(1).trim().split(whitespaces).map { it.trim() }.filter { it.isNotEmpty() })
				else ->
				{
					if (curr != null)
						students[curr.name] = curr
					curr = Student(line)
				}
			}
		}
		if (curr != null)
			students[curr.name] = curr
		logger.debug("Read ${students.size} students: ${students.keys.joinToString(", ")}")
		file.close()
	}
	
	fun process() : Processor
	{
		for (student in students.values)
		{
			// wenn der Schüler sich selbst hasst ist das nicht unser Problem
			if (student.enemies.contains(student.name))
				throw ImpossibleException()
			
			// sonst sollten wir dafür sorgen dass er einem Raum hinzugefügt wird
			if (student.room == null)
				processStudent(student)
		}
		
		return this
	}
	
	private fun processStudent(student : Student, notCareAbout : HashSet<String> = HashSet<String>())
	{
		if (student.room != null)
		{
			logger.warn("Calling processStudent for already processed student")
			return
		}
		notCareAbout.add(student.name)
		
		// wenn dieser Schüler Freunde hat, müssen diese in einem Raum sein (sofern sie bereits einen
		// Raum zugeteilt worden sind)
		val suggestedRooms = ArrayList<Room>()
		for (friend in student.friends.filter {
			!notCareAbout.contains(it)
		}.map {
			students[it] ?: throw IllegalStateException("Missing student $it")
		})
		{
			if (friend.room == null)
				processStudent(friend, notCareAbout)
			if (!suggestedRooms.contains(friend.room!!))
				suggestedRooms.add(friend.room!!)
		}
		
		// wenn es mehrere Räume mit Freunden gibt müssen diese fusioniert werden
		while (suggestedRooms.size > 1)
		{
			if (!suggestedRooms[0].canMerge(suggestedRooms[1]))
				throw ImpossibleException()
			
			suggestedRooms[0].members.addAll(suggestedRooms[1].members)
			for (member in suggestedRooms[1].members)
				member.room = suggestedRooms[0]
			suggestedRooms.removeAt(1)
		}
		
		// wenn es jetzt einen Raum mit Freunden gibt, muss der Schüler diesem beitreten
		if (suggestedRooms.size == 1)
		{
			if (suggestedRooms[0].canAdd(student))
			{
				suggestedRooms[0].members.add(student)
				student.room = suggestedRooms[0]
			}
			else
				throw ImpossibleException()
		}
		
		// ansonsten einen neuen Raum anlegen
		else
		{
			student.room = Room()
			student.room!!.members.add(student)
		}
		
		notCareAbout.remove(student.name)
	}
}

fun log(p : Processor)
{
	for (student in p.students.values)
		logger.info("${student.name} ist in ${student.room}")
}

fun main(args : Array<String>)
{
	if (args.isEmpty())
	{
		println("Please give me some input files")
		System.exit(1)
		return
	}
	
	for (file in args)
	{
		try
		{
			logger.info("Processing file $file")
			log(Processor(BufferedReader(FileReader(file))).process())
			logger.info("Processing file $file finished")
		}
		catch (ex : Exception)
		{
			logger.error("Processing file $file threw an exception", ex)
		}
	}
}
