# Schwimmbad

> Alter aller Personen: _4x13 20 2_ <br>
> Anzahl an Gutscheinen: _1_ <br>
> Ferien? [Y/n] _y_ <br> 
> Wochenende? [Y/n] _y_ <br> 
> Die folgenden Tickets sollten gekauft werden: <br>
> **1x Einzelticket Ermäßigt, 1x Familienkarte** <br>
> Alles zusammen kostet **10.5 €**

## Lösungsidee

Gefordert ist, aus einer Preisliste eines Schwimmbades den
billigsten Preis für eine Menge an Menschen zu finden. Als
erstes habe ich mir überlegt, dass es sehr kompliziert,
umständlich und unübersichtlich ist, wenn ich die Preisliste
im Algorithmus hardcode. Deshalb hab ich mir folgende
Eigenschaften überlegt, die jeden Preis der angegebenen
Preisliste repräsentiert:

 - Der Preis selbst
 - Die Menge an Kindern und Erwachsenen, die maximal mit diesem
   Ticket ins Schwimmbad gehen können.
 - Ob Kinder auch als Erwachsene durchgehen können (z.B.
   Tageskarte) oder nicht (z.B. Familienkarte)
 - Eine Funktion, die prüft, ob der Preis zum angegebenen
   Zeitpunkt (Ferien/Wochenende) gültig ist

Mithilfe dieser Abstraktion kann man das Problem nun mit einer
einfachen Tiefensuche implementieren. Ein Knoten wird dabei
durch die Anzahl an Erwachsenen, Kindern und Gutscheinen
repräsentiert. Ich bezeichne hierbei Personen über 16 als
Erwachsene, Personen unter 4 Jahren als Babies, und alles
dazwischen als Kinder. Babies kann ich bei der Tiefensuche
vernachlässigen, da diese keinen Eintritt kosten, ich muss
lediglich am Anfang einmal prüfen, dass mindestens ein
Erwachsener als Begleitperson mitgeht.

In den Ferien kann ich die Anzahl an Gutscheinen von vorne herein
auf null setzen, da Gutscheine in den Ferien nicht gelten.
Ansonsten kann ich sovielen Personen freien Eintritt geben wie
ich will, muss aber aufpassen nur einen Gutschein für eine
Preisreduzierung von 10% einzusetzen. Dabei ist die Reihenfolge,
in der ich die Gutscheine einsetze, für die Zusammensetzung
des Preises irrelevant. Daher setzte ich einfach die Anzahl an
Gutscheinen auf null, sobald ich einen Gutschein für eine
Reduzierung von 10% eingesetzt habe.

Als erstes schaue ich also, ob noch Gutscheine verfügbar sind.
Wenn ja, probiere ich je einem Kind oder einem Erwachsenen
damit freien Eintritt zu verschaffen, und rufe die Tiefensuche
damit rekursiv auf.

Anschließend kann ich über alle verfügbaren Preise iterieren
und daraus alle Möglichen Personenkonstellationen (aus
Erwachsenen und Kindern) bilden, die mit diesem Preis Eintritt
erhalten. Für diese rufe ich die Tiefensuche jeweils rekursiv
auf, dabei immer mit keinen verfügbaren Gutscheinen. Wenn
theoretisch noch mindestens ein Gutschein verfügbar wäre, kann
ich den Preis mit 0.9 multiplizieren, um 10% Rabatt durch den
Gutschein einzulösen.

Da diese Tiefensuche schnell sehr langsam werden kann, habe ich
mir überlegt, dass für die selbe Anzahl an verbleibenden Leuten
und Gutscheinen immer das gleiche Ergebnis herauskommen sollte.
Deshalb habe ich mithilfe von dynamischer Programmierung die
redundanten rekursiven Aufrufe der Tiefensuche auf ein Minimum
reduziert. Da ich nun maximal die Zustände Anzahl an Erwachsenen
`A` mal Anzahl an Kindern `C` mal Anzahl an Gutscheinen `V`
habe, und für diese maximal alle Preise `P` betrachte, ergibt
sich eine Laufzeit von `O(A*C*V*P)`.

## Umsetzung

Ich habe die Lösungsidee in Kotlin umgesetzt. Ein Preis wird
hierbei durch das Interface `IPrice` beschrieben:

```kotlin
interface IPrice
{
	val name : String
	val price : Int
	
	val adults : Int
	val children : Int
	
	val childAsAdult : Boolean
	
	fun applies(state : State) : Boolean
}
```

Dazu gibt es die Klasse `Price`, die das Interface `IPrice`
durch Konstruktor-Parameter implementiert:

```kotlin
data class Price(
	override val name : String,
	override val price : Int,
	override val adults : Int,
	override val children : Int,
	override val childAsAdult : Boolean,
	val duringHolidays : Boolean = true,
	val duringSchooldays : Boolean = true,
	val duringWeekend : Boolean = true,
	val duringWeekdays : Boolean = true
) : IPrice
{
	override fun applies(state : State) : Boolean
		= (if (state.holidays) duringHolidays else duringSchooldays)
		&& (if (state.weekend) duringWeekend else duringWeekdays)
	
	override fun toString()
		= "Price('$name': ${price/100f}€ for $adults+$children)"
}
```

Die in der Aufgabenstellung gegebene Preisliste sieht dann wie
folgt aus:

```kotlin
fun defaultPrices() : PriceList = listOf(
    Price("Einzelticket", 3 EUR 50, 1, 0, false, duringWeekdays = false),
    Price("Einzelticket Ermäßigt", 2 EUR 50, 0, 1, false, duringWeekdays = false),
    Price("Einzelticket", 2 EUR 80, 1, 0, false, duringWeekend = false),
    Price("Einzelticket Ermäßigt", 2 EUR 0, 0, 1, false, duringWeekend = false),
    Price("Tageskarte", 11 EUR 0, 6, 0, true, duringWeekend = false),
    Price("Familienkarte", 8 EUR 0, 2, 2, false),
    Price("Familienkarte", 8 EUR 0, 1, 3, false)
)
```

Den eigentlichen Algorithmus enthält die Klasse `Processor`. Sie
nimmt im Konstruktor eine Liste mit dem Alter aller Personen,
die Anzahl an Gutscheinen, den Status sowie die Preisliste des
Schwimmbads entgegen.

```kotlin
class Processor(
		val ages : Iterable<Int>,
		val vouchers : Int,
		val state : State,
		priceList : PriceList
)
```

Anschließend wird die Preisliste nach Preisen gefiltert, die auf
`state` zutreffen, sowie die Liste der Personen in die Kategorien
Baby, Kind und Erwachsener aufgeteilt:

```kotlin
init
{
	this.priceList = priceList.filter { it.applies(state) }
	
	babies = ages.filter { it < 4 }.size
	children = ages.filter { it in 4..16 }.size
	adults = ages.filter { it > 16 }.size
}
```

Die Funktion `dfs()` startet anschließend den Algorithmus. Vorher
wird noch überprüft, dass alle Babies nur in Begleitung
erscheinen.

```kotlin
fun dfs() : Result
{
	if (babies > 0 && adults == 0)
		throw IllegalStateException("Babies müssen immer in Begleitung " +
				"eines Erwachsenen sein")
	return dfs(Remaining(adults, children,
			if (state.holidays) 0 else vouchers))
}
```

Die Funktion `dfs(Remaining)` enthält den eigentlichen Algorithmus,
eine rekursive Tiefensuche mit dynamic programming:

```kotlin
private fun dfs(r : Remaining) : Result
{
```

Wenn `r` schon besucht wurde, das Ergebnis zurückgeben.

```kotlin
	if (dp.containsKey(r))
		return dp[r]!!
```

Wenn keine Personen vorhanden sind, müssen wir auch nix
bezahlen.

```kotlin
	if (r.children == 0 && r.adults == 0)
		return Result(0, emptyList())
```

Diese Variablen speichern die aktuell besten Tickets:

```
	var bestPrice = Int.MAX_VALUE
	var bestBuy : Collection<IPrice> = emptyList()
```

Wenn es noch einen Gutschein gibt, dann können wir mindestens
einer Person freien Eintritt geben:
	
```kotlin
	if (r.vouchers > 0)
	{
		// über die Anzahl an Gutscheinen für Erwachsenen iterieren
		for (a in 0 .. r.vouchers)
		{
			if (a > r.adults)
				break
			
			// über die Annzahl an Gutscheinen für Kinder iterieren
			for (c in 0 .. (r.vouchers - a))
			{
				if (a == 0 && c == 0)
					continue
				if (c > r.children)
					break
				
				val res = dfs(r.minus(a=a, c=c, v=a+c))
				if (res.price < bestPrice)
				{
					bestPrice = res.price
					bestBuy = res.toBuy
				}
			}
		}
	}
```
	
Alle Tickets durchsuchen:

```kotlin
	for (p in priceList)
	{
		// über die Anzahl an Erwachsenen iterieren
		for (a in 0 .. p.adults)
		{
			if (a > r.adults)
				break
			
			// über die Anzahl an Kindern iterieren
			for (c in 0 .. (p.children +
					if (p.childAsAdult) (p.adults - a) else 0))
			{
				if (a == 0 && c == 0)
					continue
				if (c > r.children)
					break
```

Die Tiefensuche rekursiv aufrufen, dabei die Anzahl an
Erwachsenen/Kindern, die in diesem Ticket enthalten sind,
abziehen.

```kotlin
				val res = dfs(r.minus(a=a, c=c, v=r.vouchers))
				var price = p.price + res.price
```

Eventuell einen noch vorhandenen Gutschein anwenden.

```kotlin
				if (r.vouchers > 0)
					price = ceil(price * 0.9)
```

Wennn dieses Ticket insgesammt billiger ist, die Lösung
speichern.

```kotlin
				if (price < bestPrice)
				{
					bestPrice = price
					bestBuy = LinkedList(res.toBuy)
					bestBuy.offerFirst(p)
				}
			}
		}
	}
	
	if (bestPrice == Int.MAX_VALUE)
		throw RuntimeException("Couldn't find any solution, check the " +
				"price list!")
```	
	
Das Ergebnis in `dp` speichern und zurückgeben.

```kotlin
	val res = Result(bestPrice, bestBuy)
	dp[r] = res
	return res
}
```

## Beispiele

### Antonia

Antonia aus der Aufgabenstellung sollte für insgesammt
**10,50 €** die folgenden Tickets kaufen:

 - Einzelticket Ermäßigt
 - Familienkarte

### Beispiel 1

An einem Wochenende in den Ferien müsste die Schulklasse plus
Lehrerin (alle Erwachsen) für jeden ein Einzelticket lösen, was
insgesamt **94,50 €** kosten würde. An einem Werktag in den
Ferien könnte die Gruppe für **52,40 €** 4 Tageskarten und 3
Einzeltickets lösen. Während der Schulzeit könnten zwar die
Gutscheine genutzt werden, aber da es Wochenende ist müssen
wieder alle ein Einzelticket lösen, was insgesamt **78,75 €**
kosten würde. Daher empfiehlt es sich an einem Werktag in den
Ferien ins Schwimmbad zu gehen.

### Beispiel 2

An Wochenenden sollte die Großfamilie Stutzenberg für insgesamt
**19,50 €** zwei Familienkarten und ein Einzelticket, an
Wochentage für **17,00 €** eine Tageskarte und drei ermäßigte
Einzeltickets lösen.

### Beispiel 3

Wenn beide Kegelvereine zusammen ins Schwimmbad gehen (32
Erwachsene) müssten sie **97,65 €** Eintritt bezahlen. Gingen
beide Kegelvereine mit jeweils einem Gutschein alleine ins
Schwimmbad, müssten sie **44,10 €** respektive **56,70 €**
bezahlen, also insgesammt **100,80 €**. Es werden also
**3,15 €** eingespart.
